from django.apps import AppConfig


class NepaliStatusAndQuotesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nepali_status_and_quotes'
