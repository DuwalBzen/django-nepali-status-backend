from django.forms import ModelForm
from .models import Status

class StatusForm(ModelForm):
#Here we need to specifi what class we are working with and what field we want to include
    class Meta:
        model = Status
        fields = ['status','category']
