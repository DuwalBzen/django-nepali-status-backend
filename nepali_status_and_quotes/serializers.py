from rest_framework import serializers

from .models import Status,Status_Category,UserSubmittedStatus,ImageStatus,VideoStatus

class StatusSerializer(serializers.ModelSerializer):
    # Here we describe what field we want to associate with the api
    category = serializers.SlugRelatedField(read_only=True, slug_field='category')

    class Meta:
        model = Status
        fields = ['id','status','category']

class ImageStatusSerializer(serializers.ModelSerializer):
    # Here we describe what field we want to associate with the api
    category = serializers.SlugRelatedField(read_only=True, slug_field='category')

    class Meta:
        model = ImageStatus
        fields = ['id','image_url','category']

class VideoStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoStatus
        fields = '__all__'

class UserSubmittedStatusSerializer(serializers.ModelSerializer):
    category = serializers.SlugRelatedField(read_only=True, slug_field='category')

    class Meta:
        model = UserSubmittedStatus
        fields = ['id','status','category','status_by','submited_date']
