from django.contrib import admin
from .models import Status_Category,Status,UserSubmittedStatus,ImageStatus,VideoStatus

# Register your models here.
admin.site.register(Status_Category)
admin.site.register(Status)
admin.site.register(UserSubmittedStatus)
admin.site.register(ImageStatus)
admin.site.register(VideoStatus)
