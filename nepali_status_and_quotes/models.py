from django.db import models

# Create your models here.
class Status_Category(models.Model):
    category = models.CharField(max_length=255)

    def __str__(self):
        return self.category

class Status(models.Model):
    status = models.CharField(max_length=255, unique=True)
    category = models.ForeignKey(Status_Category,on_delete=models.CASCADE)
    added_datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.status

class ImageStatus(models.Model):
    image_url = models.CharField(max_length=255)
    category = models.ForeignKey(Status_Category,on_delete=models.CASCADE)
    added_datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.image_url

class VideoStatus(models.Model):
    name = models.CharField(max_length=255)
    video_url = models.CharField(max_length=255)
    added_datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class UserSubmittedStatus(models.Model):
    status = models.CharField(max_length=512)
    category = models.ForeignKey(Status_Category,on_delete=models.CASCADE)
    status_by = models.CharField(max_length=255)
    email = models.CharField(max_length=100,blank=True, null=True)
    fcm_token = models.CharField(max_length=255,default='previous')
    submited_date = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=False)

    def __str__(self):
        return self.status
