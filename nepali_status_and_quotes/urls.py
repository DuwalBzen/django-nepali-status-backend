from django.urls import path
from nepali_status_and_quotes import views

urlpatterns = [
    path('', views.home,name='homepage'),
    path('about_us/', views.About_Us),
    path('all_status/', views.allstatus, name='allstatus'),
    path('status/<int:pk>/delete', views.deletestatus, name='deletestatus'),
    path('status/<int:status_pk>/update', views.updatestatus, name='updatestatus'),
    path('image_status/', views.imagestaus, name='imagestatus'),
    path('image/<int:img_pk>/delete', views.deleteimage, name='deleteimage'),

    path('received_status/', views.receivedstatus, name='receivedstatus'),
    path('approved_status/<int:pk>', views.approvedstatus, name='approvedstatus'),
    path('submited_status/<int:pk>/delete', views.deletesubmittedstatus, name='deletesubmittedstatus'),

    path('admin_pannel/', views.AdminPanel, name='adminpanel'),
    path('login/', views.loginuser, name='loginuser'),
    path('logout/', views.logoutuser, name='logoutuser'),

    #APIS
    path('api/status',views.StatusList.as_view()),
    path('api/image_status',views.ImageStatusData.as_view()),
    path('api/video_status',views.VideoStatus.as_view()),
    path('api/submit_status',views.SubmitStatus.as_view())
]
