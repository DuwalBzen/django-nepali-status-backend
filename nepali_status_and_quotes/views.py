from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse

#from rest rest_framework
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

#import serializers
from .serializers import StatusSerializer,UserSubmittedStatusSerializer,ImageStatusSerializer,VideoStatusSerializer

# Importing models
from .models import Status,Status_Category,ImageStatus,UserSubmittedStatus,VideoStatus

#for FCM_SERVER_KEY
from fcm_django.models import FCMDevice

#import forms
from .forms import StatusForm

#only  login user can access certain things
from django.contrib.auth.decorators import login_required

#import login to make user login
from django.contrib.auth import login, logout, authenticate

#for creating user account
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm



# Create your views here.
def home(request):
    data ={"message":"Website Under Maintainance"}
    #return render(request,'nepali_status_and_quotes/index.html',context=result)
    return render(request,'nepali_status_and_quotes/homepage.html',context=data)


def AdminPanel(request):
    return render(request,'nepali_status_and_quotes/index.html')

def About_Us(request):
    return HttpResponse("This is about us page")

@login_required
def allstatus(request):
    if request.method == "GET":
        catqueryset = Status_Category.objects.all()
        queryset = Status.objects.order_by('-id')
        return render(request,'nepali_status_and_quotes/all_status.html',{'status':queryset,'categories':catqueryset})
    else:
        catqueryset = Status_Category.objects.all()
        queryset = Status.objects.filter(category = request.POST['category']).order_by('-id')
        return render(request,'nepali_status_and_quotes/all_status.html',{'status':queryset,'categories':catqueryset})

@login_required
def updatestatus(request,status_pk):
    status = get_object_or_404(Status,pk=status_pk)
    if request.method == 'GET':
        form = StatusForm(instance=status)
        return render(request,'nepali_status_and_quotes/update_status.html',{'form':form})
    else:
        try:
            #instance=todo -> Help to know we are updating existinng obj
            form = StatusForm(request.POST, instance=status)
            form.save()
            return redirect('allstatus')
        except ValueError:
            return render(request,'nepali_status_and_quotes/update_status.html', {'form':form,'error':'Bad info'} )

@login_required
def deletestatus(request,pk):
    status = get_object_or_404(Status,pk=pk)
    if request.method == 'POST':
        status.delete()
        return redirect('allstatus')

@login_required
def imagestaus(request):
    if request.method == 'GET':
        queryset = ImageStatus.objects.all()
        return render(request,'nepali_status_and_quotes/image_status.html',{'images':queryset})

@login_required
def deleteimage(request,img_pk):
    image = get_object_or_404(ImageStatus,pk=img_pk)
    if request.method == 'POST':
        image.delete()
        return redirect('imagestatus')


@login_required
def receivedstatus(request):
    received_status = UserSubmittedStatus.objects.filter(approved = False).order_by('-id')
    return render(request,'nepali_status_and_quotes/received_status.html',{'status':received_status})

@login_required
def approvedstatus(request,pk):
    approveStatus = UserSubmittedStatus.objects.get(pk = pk)
    if request.method == "POST":
        approveStatus.approved = True
        approveStatus.save()
        device = FCMDevice()
        device.registration_id = approveStatus.fcm_token
        device.send_message(title="Hello "+approveStatus.status_by, body="Your status is approved :- " +approveStatus.status)
        return redirect('receivedstatus')

@login_required
def deletesubmittedstatus(request,pk):
    status = get_object_or_404(UserSubmittedStatus,pk=pk)
    if request.method == 'POST':
        status.delete()
        return redirect('receivedstatus')


@login_required
def deletesubmittedstatus(request,pk):
    status = get_object_or_404(UserSubmittedStatus,pk=pk)
    if request.method == 'POST':
        status.delete()
        return redirect('receivedstatus')

def loginuser(request):
    if request.method == 'GET':
        return render(request,'nepali_status_and_quotes/user_login.html', {'form':AuthenticationForm()} )
    else:
        user = authenticate(request, username =request.POST['username'] ,password=request.POST['password'])
        if user is None:
             return render(request,'nepali_status_and_quotes/user_login.html', {'form':AuthenticationForm() ,'error':'Username and password does not match'} )
        else:
            login(request,user)
            return redirect('adminpanel')

@login_required
def logoutuser(request):
    if request.method == 'POST':
        logout(request)
        return redirect('loginuser')


#API
class StatusList(generics.ListCreateAPIView):
    queryset = Status.objects.order_by('-id')
    serializer_class = StatusSerializer

    def perform_create(self,serializer):
        serializer.save(category_id=self.request.data['category'])


class ImageStatusData(generics.ListAPIView):
    queryset = ImageStatus.objects.order_by('-id')
    serializer_class = ImageStatusSerializer


class VideoStatus(generics.ListAPIView):
    queryset = VideoStatus.objects.order_by('-id')
    serializer_class = VideoStatusSerializer


class SubmitStatus(APIView):

    def get(self,request):
        user_submitted_status = UserSubmittedStatus.objects.filter(approved = True).order_by('-id')
        serializer = UserSubmittedStatusSerializer(user_submitted_status, many=True)
        return Response(serializer.data)

    def post(self,request):
        serializer = UserSubmittedStatusSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(category_id=request.data['category'])
            sucess={'success':'submitted successfully'}
            return Response(sucess, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
